//
//  ViewController.swift
//  GCDGroup
//
//  Created by 金鑫 on 2020/6/9.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        testGroup()
        
        testGroupEnterLeave()

        testGroupWorkItem()
        
    }

}

extension ViewController {

    func testGroup() {
        print("testGroup")
        
        let group = DispatchGroup()
        let queue = DispatchQueue.global()
        
        queue.async(group: group) {
            Thread.sleep(forTimeInterval: 2)
            print("Group 完成任务 1: \(Thread.current)")
        }
        
        queue.async(group: group) {
            Thread.sleep(forTimeInterval: 2)
            print("Group 完成任务 2: \(Thread.current)")
        }
        
        queue.async(group: group) {
            Thread.sleep(forTimeInterval: 2)
            print("Group 完成任务 3: \(Thread.current)")
        }
        
        group.notify(queue: DispatchQueue.main) {
            print("Group 任务 1 2 3 全部完成: \(Thread.current)")
        }
        
        queue.sync {
            print("Group 完成任务 4: \(Thread.current)")
        }
    }
    
    func testGroupEnterLeave() {
        print("testGroupEnterLeave()")
        
        let group = DispatchGroup()
        let queue = DispatchQueue.global()
        
        group.enter()
        queue.async {
            Thread.sleep(forTimeInterval: 2)
            print("EnterLeave 完成任务 1: \(Thread.current)")
            group.leave()
        }
        
        group.enter()
        queue.async {
            Thread.sleep(forTimeInterval: 2)
            print("EnterLeave 完成任务 2: \(Thread.current)")
            group.leave()
        }
        
        group.enter()
        queue.async {
            Thread.sleep(forTimeInterval: 2)
            print("EnterLeave 完成任务 3: \(Thread.current)")
            group.leave()
        }
        
        group.notify(queue: DispatchQueue.main) {
            print("EnterLeave 任务 1 2 3 全部完成: \(Thread.current)")
        }
        
        queue.async {
            print("EnterLeave 完成任务 4: \(Thread.current)")
        }
    }
    
    func testGroupWorkItem() {
        print("testGroupWorkItem()")
        
        let group = DispatchGroup()
        
        DispatchQueue.global().async(group: group, execute: DispatchWorkItem(block: {
            Thread.sleep(forTimeInterval: 2)
            print("WorkItem 完成任务 1:\(Thread.current)")
        }))
        
        DispatchQueue.global().async(group: group, execute: DispatchWorkItem(block: {
            Thread.sleep(forTimeInterval: 2)
            print("WorkItem 完成任务 2:\(Thread.current)")
        }))
        
        DispatchQueue.global().async(group: group, execute: DispatchWorkItem(block: {
            Thread.sleep(forTimeInterval: 2)
            print("WorkItem 完成任务 3:\(Thread.current)")
        }))
        
        // 非阻塞
        group.notify(queue: DispatchQueue.main) {
            print("WorkItem 任务 1 2 3 全部完成:\(Thread.current)")
        }
        
        //        group.wait()
        //        print("全部完成:\(Thread.current)")
        
        print("WorkItem 完成任务 4: \(Thread.current)")
    }
}

